# giphy-app

## The challenge

Stash has decided to pivot from investing to the GIF-search-engine space,
and needs your help to build out a web-app prototype for their new product.
Create a GIF search engine using Giphy's public API.
The app should have the following functionality:
1. The app serves a page consisting of a simple form with a text field and a
button.
2. When the user enters text, use Javascript to request some GIFs from the
Giphy API
3. When the API responds, populate the page with GIFs.
4. A user can click a GIF to add it to their "favorites".
5. A user can view another page which displays their favorite GIFs.
The UI has been left up to you as a way of showcasing your design skills.

## Run

1. `npm install`
2. `npm start`

## Tech Stack

1. React 16
2. Redux 4
3. React-router 4
4. TypeScript 
5. Webpack 4
6. Babel
7. Styled-components
8. Jest + Enzyme for testing :)

## Redux is the heart. It's all about Action processing!

![picture](assets/redux-actions.png)
